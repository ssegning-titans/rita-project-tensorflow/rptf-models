# RPTF Models
Here we'll develop models for recognizing simple things using tensorflow

- [Time of the day](time-of-the-day.ipynb)
- [Color](dominant-color.ipynb)
- Objects
