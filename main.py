import logging

from src.models.train_colors import RgbModel
from src.models.train_time import TimeModel

banner = '''\

 ,ggggggggggg,                                  ,ggggggggggg,                                                               ,ggggggggggggggg ,gggggggggggggg
dP"""88""""""Y8,          I8                   dP"""88""""""Y8,                                                   I8       dP""""""88"""""""dP""""""88""""""
Yb,  88      `8b          I8                   Yb,  88      `8b                                                   I8       Yb,_    88       Yb,_    88      
 `"  88      ,8P   gg  88888888                 `"  88      ,8P                          gg                    88888888     `""    88        `""    88      
     88aaaad8P"    ""     I8                        88aaaad8P"                           ""                       I8               88            ggg88gggg  
     88""""Yb,     gg     I8      ,gggg,gg          88"""""    ,gggggg,    ,ggggg,       gg   ,ggg,     ,gggg,    I8               88               88   8  
     88     "8b    88     I8     dP"  "Y8I          88         dP""""8I   dP"  "Y8ggg    8I  i8" "8i   dP"  "Yb   I8               88               88      
     88      `8i   88    ,I8,   i8'    ,8I          88        ,8'    8I  i8'    ,8I     ,8I  I8, ,8I  i8'        ,I8,        gg,   88         gg,   88      
     88       Yb,_,88,_ ,d88b, ,d8,   ,d8b,         88       ,dP     Y8,,d8,   ,d8'   _,d8I  `YbadP' ,d8,_    _ ,d88b,        "Yb,,8P          "Yb,,8P      
     88        Y88P""Y888P""Y88P"Y8888P"`Y8         88       8P      `Y8P"Y8888P"   888P"888888P"Y888P""Y8888PP88P""Y88         "Y8P'            "Y8P'      
                                                                                       ,d8I'                                                                
                                                                                     ,dP'8I                                                                 
                                                                                    ,8"  8I                                                                 
                                                                                    I8   8I                                                                 
                                                                                    `8, ,8I                                                                 
                                                                                     `Y8P"                                                                  
'''


def main():
    logging.basicConfig(level=logging.INFO)
    logging.info(banner)

    models = [
        # First the model to identify the time of the day
        # RgbModel(),
        TimeModel(),
    ]

    for model in models:
        print('Training model')
        model.train(download=True)

        print('Export model')
        model.export_model()

        print('Convert model')
        model.convert_to_nodejs_usable()

    print('Finished training models')


if __name__ == '__main__':
    main()
