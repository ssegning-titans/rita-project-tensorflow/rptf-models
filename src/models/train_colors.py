import logging as l
import os

import numpy as np
from PIL import Image
from keras import Sequential
from keras.layers import Conv2D, Dense, MaxPooling2D, Flatten
from keras.utils import img_to_array, to_categorical

from src.helpers.colors import colors, get_predominant_color, closest_color
from src.models.abstract_model import AbstractRptfModel

categories = len(colors.values())


class RgbModel(AbstractRptfModel):
    def __init__(self, name='rgb', num_clusters=5, dim=150, limit=100):
        self.num_clusters = num_clusters
        self.dim = dim
        self.limit = limit
        l.info(f'Labels are {colors}')
        super().__init__(name)

    def create_model(self):
        # Define the neural network model
        model = Sequential([
            Conv2D(32, (3, 3), activation='relu', input_shape=(self.dim, self.dim, 3)),
            MaxPooling2D((2, 2)),
            Conv2D(64, (3, 3), activation='relu'),
            MaxPooling2D((2, 2)),
            Conv2D(128, (3, 3), activation='relu'),
            MaxPooling2D((2, 2)),
            Flatten(),
            Dense(128, activation='relu'),
            Dense(categories, activation='softmax')
        ])

        # Compile the model with appropriate loss function, optimizer, and metrics
        model.compile(
            loss='categorical_crossentropy',
            optimizer='adam',
            metrics=['accuracy']
        )

        return model

    # Define a function to load and process images
    def process_image(self, image_path: str):
        # Load the image
        try:
            image = Image.open(image_path).convert('RGB')

            # Resize the image to reduce the number of pixels
            resized_image = image.resize((self.dim, self.dim))

            # Get the predominant color of the image
            color = get_predominant_color(resized_image)
            label = closest_color(color)

            return img_to_array(resized_image), label
        except Exception as e:
            l.error('-->', e)
            return None, None

    def data_generator(self, download=True):
        if download:
            self.download_images()

        images, labels = self.load_dataset()

        # Convert the images and labels to numpy arrays
        x = np.asarray(images)
        y = np.asarray(labels)

        return x, y

    def load_dataset(self):
        images = []
        labels = []
        for root, _, files in os.walk(f"{os.getcwd()}/downloads/{self.name}"):
            for file in files:
                if file.endswith('.jpg'):
                    filepath = os.path.join(root, file)
                    image, label = self.process_image(filepath)
                    if image is not None:
                        images.append(image)
                        labels.append(label)
        return images, labels

    def train(self, epochs=100, should_plot=False, download=False):
        l.info(f'Data generator...')
        x, y = self.data_generator(download=download)
        y = to_categorical(y, num_classes=categories)

        l.info(f'Data generated')
        # Train the model on the training set
        self.history = self.model.fit(
            x, y,
            epochs=50,
            validation_split=0.4
        )

        if should_plot:
            self.plot_history(epochs)
