import logging as l
import os

from PIL import Image
from keras import Sequential
from keras.layers import Conv2D, MaxPool2D, Flatten, Dense
from keras.preprocessing.image import ImageDataGenerator

from src.models.abstract_model import AbstractRptfModel


class TimeModel(AbstractRptfModel):
    def __init__(self, name='time', dim=150, limit=100):
        self.dim = dim
        self.limit = limit
        super().__init__(name)

    def data_generator(self, download=False, cleanup=True):
        if download:
            l.info('Downloading data')
            for x, y in ((i, j) for i in ['training', 'validation'] for j in ['night', 'afternoon', 'morning', 'noon']):
                self.download_images(
                    keywords=[f'city_at_{y}', f'street_at_{y}', f'forest_{y}'],
                    save_path=f'{x}/{y}',
                )

        if cleanup:
            for x, y in ((i, j) for i in ['training', 'validation'] for j in ['night', 'afternoon', 'morning', 'noon']):
                for root, _, files in os.walk(f"{os.getcwd()}/downloads/{self.name}/{x}/{y}"):
                    for file in files:
                        if file.endswith('.jpg'):
                            filepath = os.path.join(root, file)
                            try:
                                Image.open(filepath)
                            except Exception as e:
                                l.error(f'Error during cleaning data {e}')
                                if os.path.exists(filepath):
                                    os.remove(filepath)
                                continue

        # Create a data generator for the training data
        train_datagen = ImageDataGenerator(rescale=1. / 255)

        # Create a data generator for the validation data
        val_datagen = ImageDataGenerator(rescale=1. / 255)

        # Load the traininf data from directory
        train_generator = train_datagen.flow_from_directory(
            f'{os.getcwd()}/downloads/{self.name}/training',
            target_size=(self.dim, self.dim),
            batch_size=32,
            class_mode='categorical'
        )

        # Load the traininf data from directory
        val_generator = val_datagen.flow_from_directory(
            f'{os.getcwd()}/downloads/{self.name}/validation',
            target_size=(self.dim, self.dim),
            batch_size=32,
            class_mode='categorical'
        )

        return train_generator, val_generator

    def create_model(self):
        model = Sequential([
            Conv2D(32, (3, 3), activation='relu', input_shape=(self.dim, self.dim, 3)),
            MaxPool2D((2, 2)),
            Conv2D(64, (3, 3), activation='relu'),
            MaxPool2D((2, 2)),
            Conv2D(128, (3, 3), activation='relu'),
            MaxPool2D((2, 2)),
            Flatten(),
            Dense(64, activation='relu'),

            # We have 4 units because we four label {morning, afternoon, noon, night}
            Dense(4, activation='softmax'),
        ])
        model.compile(
            optimizer='adam',
            loss='categorical_crossentropy',
            metrics=['accuracy']
        )
        return model

    def train(self, epochs=100, should_plot=False, download=False):
        train_generator, val_generator = self.data_generator(download=download)

        # Train the model
        self.history = self.model.fit(
            train_generator,
            epochs=epochs,
            validation_data=val_generator
        )

        if should_plot:
            self.plot_history(epochs)
