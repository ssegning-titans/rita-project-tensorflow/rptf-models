import logging
import multiprocessing
from os import getcwd, cpu_count
from os.path import isfile
from subprocess import run, PIPE

from google_images_download.google_images_download import googleimagesdownload
from keras import models
from keras.engine.functional import Functional

from src.helpers.dir import output_dir, ensure_dir
from src.helpers.download_utils import download_a_file
from src.helpers.ploting import plot


class AbstractRptfModel:
    def __init__(self, name):
        self.name = name
        self.model = self.load_model()
        self.history = None

        if self.model is None:
            self.model = self.create_model()

    def download_images(self, keywords=None, save_path=''):
        # download the images
        if keywords is None:
            keywords = ['city', 'street', 'night-city', 'house', 'monument', 'oldenburg', 'bremen']

        response = googleimagesdownload()
        for keyword in keywords:
            images, tabs = response.download_page(
                f'https://www.google.com/search?q={keyword}&espv=2&biw=1366&bih=667&site=webhp&source=lnms&tbm=isch'
                f'&tbs=&sa=X&ei=XosDVaCXD8TasATItgE&ved=0CAcQ_AUoAg')
            paths = [i[1][3][0] for i in images]

            save_dir = f"{getcwd()}/downloads/{self.name}/{save_path}"
            ensure_dir(save_dir)

            # Download images using multiple thread
            with multiprocessing.Pool(cpu_count()) as pool:
                pool.map(download_a_file,
                         [{'path': path, 'i': i, 'save_dir': save_dir, 'keyword': keyword, 'name': self.name}
                          for i, path in
                          enumerate(paths)])

    def summary(self):
        # Show the model architecture
        self.model.summary()

    def _save_folder(self):
        return output_dir() + '/' + self.name

    def _save_path(self):
        sf = self._save_folder() + '/model.h5'
        return sf

    def convert_to_nodejs_usable(self, input_format="keras"):
        sp = self._save_path()
        o_path = self._save_folder() + '/tfjs_model'

        command = [
            "tensorflowjs_converter",
            "--input_format={}".format(input_format),
            "{}".format(sp),
            "{}".format(o_path),
        ]
        process = run(command, stdout=PIPE, stderr=PIPE)
        if process.stderr:
            logging.error(process.stderr.decode())

    def export_model(self):
        sp = self._save_path()
        self.model.save(sp)

    def plot_history(self, epochs=10):
        plot(self.history, epochs)

    def create_model(self) -> Functional:
        raise NotImplementedError("Please Implement this method")

    def train(self, epochs=100, should_plot=False, download=False) -> Functional:
        raise NotImplementedError("Please Implement this method")

    def load_model(self) -> Functional:
        s_path = self._save_path()

        if isfile(s_path):
            # Recreate the exact same model, including its weights and the optimizer
            model = models.load_model(s_path)
            print('Model found at [{0}]:'.format(s_path))

            return model
