from os import getcwd
from pathlib import Path


def ensure_dir(path_url: str) -> None:
    path = Path(path_url)
    path.mkdir(parents=True, exist_ok=True)


def output_dir() -> str:
    o = getcwd() + '/output'
    ensure_dir(o)
    return o


def data_dir() -> str:
    o = getcwd() + '/downloads'
    return o

