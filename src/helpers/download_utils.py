import logging
import socket
import urllib
import urllib.error
import urllib.request


def download_a_file(d: dict):
    opener = urllib.request.build_opener()
    opener.addheaders = [('User-Agent',
                          'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) '
                          'Chrome/36.0.1941.0 Safari/537.36')]
    urllib.request.install_opener(opener)
    socket.setdefaulttimeout(30)

    path = d['path']
    save_dir = d['save_dir']
    keyword = d['keyword']
    i = d['i']
    name = d['name']

    try:
        img_path = f"{save_dir}/{keyword}_{i + 1}.jpg"
        urllib.request.urlretrieve(url=path, filename=img_path)
    except Exception as e:
        logging.error(f'[{name}] - {keyword}: [{i}] {path} {e}')
