# Define a function to get the predominant color of an image
import numpy as np
import webcolors
from sklearn.cluster import KMeans

_colors = {
    '#000000': 'black',
    '#0000ff': 'blue',
    '#008000': 'green',
    '#800080': 'purple',
    '#ff0000': 'red',
    '#ffffff': 'white',
    '#ffff00': 'yellow'
}
colors = {c: idx for idx, c in enumerate(_colors.values())}


def get_predominant_color(image):
    # Convert the image to a numpy array
    pixel_data = np.array(image)

    # Reshape the pixel data to a 2D array of pixels
    pixel_data = pixel_data.reshape((-1, 3))

    # Use K-means clustering to find the predominant color
    kmeans = KMeans(n_clusters=1, random_state=0)
    kmeans.fit(pixel_data)
    predominant_color = kmeans.cluster_centers_[0].astype(int)
    return tuple(predominant_color)


def closest_color(rgb: tuple) -> tuple:
    min_colors = {}
    for key, name in _colors.items():
        r_c, g_c, b_c = webcolors.hex_to_rgb(key)
        rd = (r_c - rgb[0]) ** 2
        gd = (g_c - rgb[1]) ** 2
        bd = (b_c - rgb[2]) ** 2
        min_colors[(rd + gd + bd)] = name
    color = min_colors[min(min_colors.keys())]
    return colors[color]
